import Quick
import Nimble

class DummySpec: QuickSpec {
    override func spec() {
        describe("this dummy spec") {
            it("will succeed indefinitely") {
                expect("abcdefg").to(contain("cde"))
            }
            
//            it("will fail indefinitely") {
//                expect("abcdefg").to(contain("zzz"))
//            }
        }
    }
}
